ORDERING = [-8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8]


class User(object):

    def __init__(self, rank=-8, progress=0):
        self.progress = progress

        if rank in ORDERING:
            self.rank = rank
        else:
            raise ValueError("Invalid Rank")

    def inc_progress(self, activity_rank):
        current_index = ORDERING.index(self.rank)
        activity_index = ORDERING.index(activity_rank)

        diff = activity_index - current_index

        if diff > 0:
            # Progressing upwards
            total_progress = self.progress + (10 * diff * diff)

            new_index = current_index + (total_progress // 100)

            self.rank = ORDERING[new_index]
            self.progress = total_progress % 100

            if self.rank == 8:
                # if we exceed the limit, reset the progress
                self.progress = 0
        elif diff == 0:
            # The activity is ranked the same as the current user's rank
            total_progress = self.progress + 3

            self.rank += (total_progress // 100)
            self.progress = total_progress % 100

            if self.rank == 8:
                self.progress = 0
        elif diff == -1:
            # One level lower
            total_progress = self.progress + 1

            self.rank += (total_progress // 100)
            self.progress = total_progress % 100

            if self.rank == 8:
                self.progress = 0
        else:
            # Progressing downwards more than 1 level
            None
