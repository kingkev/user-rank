import pytest

from user import User


class TestUser:
    def test_creating_user(self):
        user = User()
        assert user.rank == -8
        assert user.progress == 0


class TestInvalidInputs:
    def test_create_user_rejects_invalid_rank(self):
        with pytest.raises(ValueError):
            user = User(9)

    def test_create_user_rejects_invalid_activity(self):
        user = User()
        with pytest.raises(ValueError):
            user.inc_progress(-9)


class TestNegativeRanks:

    def test_ignores_activity_more_than_one_ranking_lower(self):
        """
        If a user ranked -1 completes an activity ranked -3 their progress remains unchanged
        """
        user = User(-1)
        user.inc_progress(-3)
        assert user.progress == 0

    def test_complete_activity_one_ranking_lower(self):
        """
        If a user ranked -1 completes an activity ranked -2 they will receive 1 progress
        """
        user = User(-1)
        user.inc_progress(-2)
        assert user.progress == 1

    def test_increase_user_progress_same_level(self):
        """
        If a user ranked -8 completes an activity ranked -8 they will receive 3 progress
        """
        user = User()
        user.inc_progress(-8)
        assert user.progress == 3

    def test_increase_user_progress_one_level(self):
        """
        If a user ranked -8 completes an activity ranked -7 they will receive 10 progress
        """
        user = User()
        user.inc_progress(-7)
        assert user.progress == 10

    def test_increase_user_progress_two_levels(self):
        """
        If a user ranked -8 completes an activity ranked -6 they will receive 40 progress
        """
        user = User()
        user.inc_progress(-6)
        assert user.progress == 40

    def test_increase_user_progress_three_levels(self):
        """
        If a user ranked -8 completes an activity ranked -5 they will receive 90 progress
        """
        user = User()
        user.inc_progress(-5)
        assert user.progress == 90

    def test_transition_from_negative_to_positive_ranks(self):
        """
        Test progressing from rank -1 to rank 4, skipping 0
        """
        user = User(-1)
        user.inc_progress(4)  # increase by 4 steps -> 160
        assert user.progress == 60
        assert user.rank == 1

    def test_sequence_of_steps(self):
        user = User()
        assert user.rank == -8
        assert user.progress == 0
        user.inc_progress(-7)
        assert user.progress == 10
        user.inc_progress(-5)  # will add 90 progress
        assert user.progress == 0  # progress is now zero
        assert user.rank == -7


class TestPositiveRanks:

    def test_ignores_activity_more_than_one_ranking_lower(self):
        """
        If a user ranked 1 completes an activity ranked -2 their progress remains unchanged
        """
        user = User(1)
        user.inc_progress(-2)
        assert user.progress == 0

    def test_complete_activity_one_ranking_lower(self):
        """
        If a user ranked 1 completes an activity ranked -1 they will receive 1 progress
        """
        user = User(1)
        user.inc_progress(-1)
        assert user.progress == 1

    def test_increase_user_progress_same_level(self):
        """
        If a user ranked 1 completes an activity ranked 1 they will receive 3 progress
        """
        user = User(1)
        user.inc_progress(1)
        assert user.progress == 3

    def test_increase_user_progress_one_level(self):
        """
        If a user ranked 1 completes an activity ranked 2 they will receive 10 progress
        """
        user = User(1)
        user.inc_progress(2)
        assert user.progress == 10

    def test_increase_user_progress_two_levels(self):
        """
        If a user ranked 1 completes an activity ranked 3 they will receive 40 progress
        """
        user = User(1)
        user.inc_progress(3)
        assert user.progress == 40

    def test_increase_user_progress_three_levels(self):
        """
        If a user ranked 1 completes an activity ranked 4 they will receive 90 progress
        """
        user = User(1)
        user.inc_progress(4)
        assert user.progress == 90

    def test_increases_ranking_when_progress_exceeds_100(self):
        """
        If a user ranked 1 completes an activity ranked 5 they will receive 160 progress
        They should also be bumped up to the next rank
        """
        user = User(1)
        user.inc_progress(5)
        assert user.progress == 60
        assert user.rank == 2


class TestUpperLimit:
    def test_cannot_exceed_maximum_rank_with_same_rank_activity(self):
        """
        If a user ranked 8 completes an activity, they do not receive progress points
        """
        user = User(8)
        user.inc_progress(8)
        assert user.progress == 0
